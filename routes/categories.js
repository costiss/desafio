const express = require('express');
const router = express.Router();
const controller = require("../controllers/categoryController.js")

router.get("/", controller.category_list )
router.get("/:id", controller.category_detail)
router.post("/", controller.category_create)
router.delete("/:id", controller.category_delete)
router.patch("/:id", controller.category_update)
module.exports = router;