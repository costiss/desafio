const express = require('express');
const router = express.Router();
const controller = require("../controllers/productController.js")



router.get("/", controller.product_list)
router.get("/:SKU", controller.product_detail)
router.post("/", controller.product_create)
router.delete("/:SKU", controller.product_delete)
router.patch("/:SKU", controller.product_update)

//router.put("/:SKU", controller.product_put)

module.exports = router