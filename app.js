const express = require("express");
const bodyParser = require('body-parser')
const morgan = require("morgan");
const cors = require('cors');


const productRoute = require("./routes/products.js")
const categoryRoute = require("./routes/categories.js")
const app = express();

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(cors())
app.use(morgan("dev"))



//Routes 
app.use("/products", productRoute)
app.use("/categories", categoryRoute)

//Error Handling
app.use((req, res, next) => {
    const error = new Error("Not Found");
    error.status = 404;
    next(error);
})

app.use((error, req, res, next) => {
    res.status(error.status || 300);
    res.json({
        error: {
            message: error.message
        }
    });
});


module.exports = app;