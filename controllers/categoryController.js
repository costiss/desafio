const { response } = require("express")
const connection = require("../config/mysqlConf.js")
const table = "categories"

exports.category_list = (req, res) => {
    connection.query(`SELECT category FROM ${table}`, (err, result) => {
        if (result.length > 0) {
            const value = []
            for (let i = 0; i < result.length; i++) {
                value.push(Object.values(result[i])[0])
            }
            res.status(200).json(value)
        } else {
            res.status(500).json({ err })
        }
    })
}

exports.category_detail = (req, res) => {
    const id = req.params.id
    connection.query(`SELECT * from ${table} where id = ?`, [id], (err, result) => {
        if (result.length > 0) {
            res.status(200).json(result[0])
        } else {
            res.status(500).json({
                err
            })
        }
    })
}

exports.category_create = (req, res) => {
    const category = req.body.category
    const id = req.body.id
    const values = [category]
    id ? values.push(id) : "" //id is optional. Otherwise, it will auto increment

    connection.query(`INSERT INTO ${table} (category${id ? ",id" : ""}) VALUES(
        ? ${id ? ", ?" : ""}
       
    )`, values, (err, result) => {
        if (err) return res.status(500).json({ err })
        if (result) return res.status(200).json({ message: "Suceffuly created" })
    })
}

exports.category_delete = (req, res) => {
    const id = req.params.id
    connection.query(`DELETE FROM ${table} where id = ?`, [id], (err, result) => {
        if (err) return res.status(500).json({ err })
        if (result) return res.status(200).json({ message: "Suceffuly deleted" })
    })
}

exports.category_update = (req, res) => {
    const id = req.params.id
    connection.query(`SELECT * FROM ${table} where id = ?`, [id], (err, result) => {
        if (result.length > 0) {
            const entry = Object.entries(result[0])
            Object.entries(req.body).forEach((value) => {
                for (let i = 0; i < entry.length; i++) {
                    if (value[0] == entry[i][0]) {
                        entry[i][1] = value[1]
                    }
                }
            })
            connection.query(`UPDATE ${table} SET
            ${entry[0][0]} = ?,
            ${entry[1][0]} = ?
            where id = ?`, [entry[0][1], entry[1][1], id], (error, response) => {
                if (error) return res.status(500).json({ error })
                if (response) return res.status(200).json({ message: "Suceffuly updated" })
            })
        }
    })
}