const connection = require("../config/mysqlConf.js")
const table = "products"

exports.product_list = (req, res) => {
    connection.query(`SELECT * FROM ${table}`, (err, result) => {
        if (result.length > 0) {
            return res.status(200).json(result)
        } else {
            return res.status(500)
        }
    })
}

exports.product_detail = (req, res) => {
    const sku = req.params.SKU
    connection.query(`SELECT * FROM ${table} where SKU = ?`, sku, (err, result) => {
        if (result.length > 0) {
            return res.status(200).json(result)
        } else {
            return res.status(500).json(
                { message: err }
            )
        }
    })
}

exports.product_create = (req, res) => {
    const form = req.body
    connection.query(`INSERT INTO ${table}  (name, SKU, description, quantity, price, category) VALUES (
       ?,?,?,?,?,?
    )`, [form.name, form.SKU, form.description, form.quantity, form.price, form.category], (err, result) => {
        if (err) return res.status(500).json({ err })
        if (result) return res.status(200).json({ message: "Suceffuly created" })
    })
}

exports.product_delete = (req, res) => {
    const sku = req.params.SKU
    connection.query(`DELETE FROM ${table} where SKU = ?`, [sku], (err, result) => {
        if (err) return res.status(500).json({ err })
        if (result) return res.status(200).json({ message: "Suceffuly deleted" })
    })

}

exports.product_update = (req, res) => {
    const sku = req.params.SKU
    connection.query(`SELECT * FROM ${table} where SKU = ?`, [sku], (err, result) => {
        if (result.length > 0) {
            const entry = Object.entries(result[0])
            Object.entries(req.body).forEach((value) => {
                for (let i = 0; i < entry.length; i++) {
                    if (value[0] == entry[i][0]) {
                        entry[i][1] = value[1]
                    }
                }
            })
            connection.query(`UPDATE ${table} SET 
            ${entry[0][0]} = ?,
            ${entry[1][0]} = ?,
            ${entry[2][0]} = ?,
            ${entry[3][0]} = ?,
            ${entry[4][0]} = ?,
            ${entry[5][0]} = ?
            WHERE SKU = ?`, [entry[0][1], entry[1][1], entry[2][1], entry[3][1], entry[4][1], entry[5][1], sku], (error, response) => {
                if (error) return res.status(500).json({ error })
                if (response) return res.status(200).json({ message: "Suceffuly updated" })
            })

        }
    })
}

// exports.product_put = (req, res) => {
//         const sku = req.params.SKU
//         const form = req.body
//         connection.query(`UPDATE ${table} SET
//             name = ?,
//             SKU = ?,
//             description = ?,
//             quantity = ?,
//             price = ?,
//             category = ?,
//             WHERE SKU = ?`, [form.name, form.SKU, form.description, form.quantity, form.price, form.category], (err, result) => {
//             if (errors) return res.status(500).json({ error: errors })
//             if (result) return res.status(200).json({ message: "Suceffuly updated" })
//         })
// }