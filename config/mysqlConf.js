const mysql = require("mysql");
require('dotenv').config()


const connection = mysql.createConnection({
    host: process.env.db_host,
    user: process.env.db_user,
    password: process.env.db_pass,
    database: process.env.db
})

//"Models"
connection.query(`CREATE TABLE IF NOT EXISTS products (
    name varchar(100),
    SKU varchar(9) not null,
    description TEXT,
    quantity INT unsigned,
    price decimal(7,2),
    category varchar(20),
    Primary key (SKU)

    );`, function (error) {
    if (error) console.log(error)

})

connection.query(`CREATE TABLE IF NOT EXISTS categories (
    id int not null unique auto_increment,
    category varchar(50) unique,
    primary key(id)

);`, function (error) {
    if (error) console.log(error)

})

module.exports = connection;