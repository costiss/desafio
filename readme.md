# Node.js Api - Desafio
## Summary
The API endpoint was developed with Express.js server and MySql database
## Database
### Tables
- **products** 
- **categories** 
## Products
### Endpoints
- **GET /products/**        - Lists all products available 
- **GET /products/:SKU**    - Detailed info about product, requires SKU as URL param
- **POST /products/**       - Create new Product
- **DELETE /products/:SKU** - Delete Product
- **PATCH /products/**      - Update product

#### Request Body - Model
    name varchar(100) DEFAULT NULL,
    SKU varchar(9) NOT NULL,
    description text,
    quantity int unsigned DEFAULT NULL,
    price decimal(7,2) DEFAULT NULL,
    category varchar(20) DEFAULT NULL //category ID spaced by | ("1|2|3")
## Categories
### Endpoints
- **GET /categories/**        - Lists all categories available 
- **GET /categories/:id**    - Detailed info about category, requires id as URL param
- **POST /categories/**       - Create new category
- **DELETE /categories/:id** - Delete category
- **PATCH /categories/**      - Update category
### Request Body - Model
    id int NOT NULL AUTO_INCREMENT,
    category varchar(50) DEFAULT NULL

## Code
 Routes are defined on **routes folder** and each controller, respectively, defined on **controllers folder** by **{routname}Controller**.
 Each controller have a function for each **CRUD** functionality

## Logging
**morgan** dependency logs every request
